package com.kas.lookaround.service;

import com.kas.lookaround.eao.UserEAO;
import com.kas.lookaround.entity.User;
import com.kas.lookaround.entity.UserDetails;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author Aliaksei Kasiyanik
 */
@Service
public class UserService  {

    @Autowired
    private UserEAO userEAO;

    @Transactional(readOnly = true)
    public User findUserByUsername(String username) {
        return userEAO.getByUsername(username);
    }

    @Transactional
    public void createUser(User user) {
        userEAO.save(user);
    }

}
