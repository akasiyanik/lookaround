package com.kas.lookaround.entity.enums;

/**
 * @author Aliaksei Kasiyanik
 */
public enum UserRole {
    ROLE_USER("USER"),
    ROLE_CUSTOMER("CUSTOMER");

    private final String value;

    UserRole(String value) {
       this.value = value;
    }

    public String getValue() {
        return value;
    }
}
