package com.kas.lookaround.config.root;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 *
 * The root context configuration of the application - the beans in this context will be globally visible
 * in all servlet contexts.
 *
 * @author akasiyanik
 *         8/4/15
 */

@Configuration
@ComponentScan("com.kas.lookaround")
public class RootContextConfig {

}
