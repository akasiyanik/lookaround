package com.kas.lookaround.config;

import com.kas.lookaround.init.TestDataInitializer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 * @author akasiyanik
 *         8/4/15
 */
@Configuration
@ComponentScan({"com.kas.lookaround.service", "com.kas.lookaround.assembler"})
public class ServiceContextConfig {

    @Bean(initMethod = "init")
    public TestDataInitializer testDataInitializer() {
        return new TestDataInitializer();
    }

}
