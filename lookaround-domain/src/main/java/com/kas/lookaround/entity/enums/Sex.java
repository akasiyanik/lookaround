package com.kas.lookaround.entity.enums;

/**
 * @author Aliaksei Kasiyanik
 */
public enum Sex {
    MALE,
    FEMALE
}
