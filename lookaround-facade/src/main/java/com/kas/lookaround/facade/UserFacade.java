package com.kas.lookaround.facade;

import com.kas.lookaround.assembler.UserAssembler;
import com.kas.lookaround.common.dto.UserDTO;
import com.kas.lookaround.eao.UserEAO;
import com.kas.lookaround.entity.User;
import com.kas.lookaround.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author Aliaksei Kasiyanik
 */
@Service
@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
public class UserFacade {

    @Autowired(required = false)
    private UserEAO userEAO;

    @Autowired
    private UserAssembler userAssembler;

    @Autowired
    private UserService userService;

    public UserDTO addUser(UserDTO entityDTO) {
        User entity = userAssembler.fromDTO(entityDTO);
        entity = userEAO.save(entity);
        return userAssembler.toDTO(entity);
    }

    public UserDTO getUser(Long id) {
        User user = userEAO.getById(id);
        return userAssembler.toDTO(user);
    }

    public UserEAO getUserEAO() {
        return userEAO;
    }

    public void setUserEAO(UserEAO userEAO) {
        this.userEAO = userEAO;
    }

    public UserAssembler getUserAssembler() {
        return userAssembler;
    }

    public void setUserAssembler(UserAssembler userAssembler) {
        this.userAssembler = userAssembler;
    }

    public UserService getUserService() {
        return userService;
    }

    public void setUserService(UserService userService) {
        this.userService = userService;
    }
}
