package com.kas.lookaround.init;

import com.kas.lookaround.eao.UserEAO;
import com.kas.lookaround.entity.User;
import com.kas.lookaround.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @author akasiyanik
 *         8/5/15
 */
@Component
public class TestDataInitializer {

    @Autowired
    private UserService userService;

    public void init() throws Exception {
        User user = new User("test123", "test@email.com", "$2a$10$x9vXeDsSC2109FZfIJz.pOZ4dJ056xBpbesuMJg3jZ.ThQkV119tS");
        userService.createUser(user);
    }
}
