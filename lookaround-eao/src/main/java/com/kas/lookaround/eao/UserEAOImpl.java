package com.kas.lookaround.eao;

import com.kas.lookaround.entity.User;
import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author Aliaksei Kasiyanik
 */
@Repository
public class UserEAOImpl extends HibernateEAO<User> implements UserEAO {

    @Autowired
    public UserEAOImpl(SessionFactory sessionFactory) {
        super(sessionFactory);
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<User> getAll() {
        Criteria criteria = super.createCriteria();
        return criteria.list();
    }

    @Override
    public User getByUsername(String username) {
        Criteria criteria = createCriteria().add(Restrictions.eq("username", username));
        return  (User) criteria.uniqueResult();
    }

}
