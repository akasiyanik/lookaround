package com.kas.lookaround.eao;

import com.kas.lookaround.entity.User;

import java.util.List;

/**
 * @author Aliaksei Kasiyanik
 */
public interface UserEAO extends EAO<User> {

    List<User> getAll();

    User getByUsername(String username);
}
