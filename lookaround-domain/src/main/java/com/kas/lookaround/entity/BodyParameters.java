package com.kas.lookaround.entity;

import java.util.Date;

/**
 * @author Aliaksei Kasiyanik
 */
public class BodyParameters extends BaseEntity {

    private Double weight;

    private Double height;

    private Date date;

    private Long userId;

    public Double getWeight() {
        return weight;
    }

    public void setWeight(Double weight) {
        this.weight = weight;
    }

    public Double getHeight() {
        return height;
    }

    public void setHeight(Double height) {
        this.height = height;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }
}
