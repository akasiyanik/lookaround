package com.kas.lookaround.core;


/**
 * Extends Spring Security User entity
 *
 * @author Aliaksei Kasiyanik
 */
public class CustomUser {

    private Long userId;

//    public CustomUser(String username, String password, Collection<? extends GrantedAuthority> authorities, Long userId) {
//        super(username, password, authorities);
//        this.userId = userId;
//    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }
}
