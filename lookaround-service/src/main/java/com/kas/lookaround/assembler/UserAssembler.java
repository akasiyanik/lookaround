package com.kas.lookaround.assembler;

import com.kas.lookaround.common.dto.UserDTO;
import com.kas.lookaround.entity.User;
import org.springframework.stereotype.Component;

/**
 * @author Aliaksei Kasiyanik
 */

@Component
public class UserAssembler extends GenericAssembler<User, UserDTO> {

    @Override
     public UserDTO toDTO(User entity) {
        return super.toDTO(entity);
    }
}
