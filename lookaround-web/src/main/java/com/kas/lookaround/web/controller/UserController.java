package com.kas.lookaround.web.controller;

import com.kas.lookaround.common.dto.UserDTO;
import com.kas.lookaround.facade.UserFacade;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

/**
 * @author Aliaksei Kasiyanik
 */
@Controller
@RequestMapping(value = "/user")
public class UserController {

    @Autowired
    private UserFacade userFacade;

    @RequestMapping(method = RequestMethod.POST)
    public @ResponseBody UserDTO addUser(@RequestBody UserDTO userDTO) {
        return userFacade.addUser(userDTO);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public @ResponseBody UserDTO getUser(@PathVariable final Long id) {
        return userFacade.getUser(id);
    }

}

