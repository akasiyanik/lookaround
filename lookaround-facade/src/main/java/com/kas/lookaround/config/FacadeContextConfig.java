package com.kas.lookaround.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 * @author akasiyanik
 *         8/4/15
 */
@Configuration
@ComponentScan("com.kas.lookaround.facade")
public class FacadeContextConfig {
}
