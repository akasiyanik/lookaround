package com.kas.lookaround.eao;

import com.kas.lookaround.entity.BaseEntity;

/**
 * @author Aliaksei Kasiyanik
 */
public interface EAO<T extends BaseEntity> {

    T save(T entity);

    void update(T entity);

    void delete(T entity);

    T getById(Long id);
}
